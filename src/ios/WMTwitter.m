#import "WMTwitter.h"
#import <Cordova/CDV.h>
#import <Social/Social.h>
#import <Accounts/Accounts.h>

@implementation WMTwitter

- (void)echo:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;
    NSString* echo = [command.arguments objectAtIndex:0];
    
    if (echo != nil && [echo length] > 0) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:echo];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (BOOL)userHasAccessToTwitter
{
    return [SLComposeViewController
            isAvailableForServiceType:SLServiceTypeTwitter];
}

//- (void)test:(CDVInvokedUrlCommand*)command
//{
//    
// 
//
//    NSString* message = [command.arguments objectAtIndex:0];
//    
//    NSString* type = SLServiceTypeTwitter;
//    NSURL* postUrl = [NSURL URLWithString:@"https://api.twitter.com/1.1/statuses/update.json"];
//    NSDictionary* postData = @{
//        @"status":message
//    };
//    
//    SLRequest* request = [SLRequest requestForServiceType:type requestMethod:SLRequestMethodPOST URL:postUrl parameters:postData];
//    ACAccountStore *accountStore = [[ACAccountStore alloc] init];
////    ACAccountType *twitterType =
////    [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
//    ACAccountType *twitterType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
//    
//    NSArray *accounts = [accountStore accountsWithAccountType:twitterType];
//    
//    [accountStore requestAccessToAccountsWithType:twitterType options: NULL completion:^(BOOL granted, NSError *error) {
//        CDVPluginResult* pluginResult = nil;
//        if(granted){
//            NSLog(@"Account count: %d",accounts.count);
//            
//            SLRequestHandler requestHandler =
//            ^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
//                CDVPluginResult* pluginResult = nil;
//                if (responseData) {
//                    NSInteger statusCode = urlResponse.statusCode;
//                    if (statusCode >= 200 && statusCode < 300) {
//                        NSDictionary *postResponseData =
//                        [NSJSONSerialization JSONObjectWithData:responseData
//                                                        options:NSJSONReadingMutableContainers
//                                                          error:NULL];
//                        NSLog(@"[SUCCESS!] Created Tweet with ID: %@", postResponseData[@"id_str"]);
//                        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsBool:true];                    }
//                    else {
//                        NSLog(@"[ERROR] Server responded: status code %d %@", statusCode,
//                              [NSHTTPURLResponse localizedStringForStatusCode:statusCode]);
//                        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsBool:false];
//                    }
//                }
//                else {
//                    NSLog(@"[ERROR] An error occurred while posting: %@", [error localizedDescription]);
//                    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsBool:false];
//                }
//                [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
//            };
//            
//            [request setAccount:[accounts lastObject]];
//            [request performRequestWithHandler:requestHandler];
//        }
//        else {
//            // Access was not granted, or an error occurred
//            NSLog(@"%@", [error localizedDescription]);
//            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsBool:false];
//        }
//        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
//    }];
//    
//    
//
////    SLComposeViewController *composeViewController = [SLComposeViewController composeViewControllerForServiceType:type];
////    [composeViewController setInitialText:message];
//    
////    [self.viewController presentViewController:composeViewController animated:YES completion:nil];
////    
//
//}

//- (void)getShortURLLength:(CDVInvokedUrlCommand*)command{
//    https://api.twitter.com/1.1/help/configuration.json
//    https://dev.twitter.com/docs/api/1.1/get/help/configuration
//}

- (void)sendTweet:(CDVInvokedUrlCommand*)command{
    if ([self userHasAccessToTwitter]) {
        ACAccountStore *accountStore = [[ACAccountStore alloc] init];
        // Create an account type that ensures Twitter accounts are retrieved.
        ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
        [accountStore requestAccessToAccountsWithType:accountType options: NULL completion:^(BOOL granted, NSError *error) {
            CDVPluginResult* pluginResult = nil;
            if(granted) {
//                NSString *message = @"Test message";
                NSURL* postUrl = [NSURL URLWithString:@"https://api.twitter.com/1.1/statuses/update.json"];
                NSString *message   = [command.arguments objectAtIndex:0];
//                NSString *fileURL = [command.arguments objectAtIndex:1];
//                NSString *linkURL = [command.arguments objectAtIndex:2];
                NSDictionary* params = @{
                    @"status":message
                };
                SLRequest* request = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodPOST URL:postUrl parameters:params];
                
                NSArray *accountsArray = [accountStore accountsWithAccountType:accountType];
                [request setAccount:[accountsArray lastObject]];
                SLRequestHandler requestHandler =
                ^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
                    CDVPluginResult* pluginResult = nil;
                    if (responseData) {
                        NSInteger statusCode = urlResponse.statusCode;
                        if (statusCode >= 200 && statusCode < 300) {
                            NSDictionary *postResponseData =
                            [NSJSONSerialization JSONObjectWithData:responseData
                                                            options:NSJSONReadingMutableContainers
                                                              error:NULL];
                            NSLog(@"[SUCCESS!] Created Tweet with ID: %@", postResponseData[@"id_str"]);
                            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:postResponseData[@"id_str"]];                    }
                        else {
                            NSLog(@"[ERROR] Server responded: status code %d %@", statusCode,
                                  [NSHTTPURLResponse localizedStringForStatusCode:statusCode]);
                            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsBool:false];
                        }

                    }else{
                        NSLog(@"[ERROR] An error occurred while posting: %@", [error localizedDescription]);
                        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsBool:false];
                    }
                    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
                };
                [request performRequestWithHandler:requestHandler];
            }else{
                pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsBool:false];
                [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
            }
        }];
    }else{
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsBool:false];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void) getTwitterUsername:(CDVInvokedUrlCommand*)command {
    if ([self userHasAccessToTwitter]) {
        ACAccountStore *accountStore = [[ACAccountStore alloc] init];
        
        // Create an account type that ensures Twitter accounts are retrieved.
        ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
        
        [accountStore requestAccessToAccountsWithType:accountType options: NULL completion:^(BOOL granted, NSError *error) {
            CDVPluginResult* pluginResult = nil;
            if(granted) {
                
                NSArray *accountsArray = [accountStore accountsWithAccountType:accountType];
                NSString *twitterUsername = nil;
                
                for (ACAccount *account in accountsArray ) {
                    twitterUsername = account.username;
                }
                
                NSLog(@"Accounts array: %d", accountsArray.count);
                
                NSLog (@"%@", twitterUsername);
                if (twitterUsername == nil) twitterUsername = @"undefined";
                NSMutableDictionary* twitterData = [NSMutableDictionary dictionaryWithCapacity:1];
                [twitterData setObject:twitterUsername forKey:@"username"];
                
                pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:twitterData];
                
            }else{
                // Access was not granted, or an error occurred
                NSLog(@"%@", [error localizedDescription]);
                pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsBool:false];
            }
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    }else{
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsBool:false];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
    
    
}

@end