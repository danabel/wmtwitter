#import <Cordova/CDV.h>

@interface WMTwitter : CDVPlugin

- (void)getTwitterUsername:(CDVInvokedUrlCommand*)command;
- (void)sendTweet:(CDVInvokedUrlCommand*)command;

@end