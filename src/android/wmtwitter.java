package uk.co.webmarmalade.wmtwitter;

import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import twitter4j.Status;
import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;

/**
 * This class echoes a string called from JavaScript.
 */
public class wmtwitter extends CordovaPlugin {
    private static final String LOG_TAG = "WM Twitter";
    private static final String TAG = "WM Twitter";
    private RequestToken requestToken = null;
    private AccessToken accessToken = null;
    private Twitter twitter = null;
    
    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if(action.equals("init")){
            String consumerKey = args.getString(0) != null ? args.getString(0) : "";
            String consumerSecret = args.getString(1) != null ? args.getString(1) : "";
            this.init(consumerKey, consumerSecret, callbackContext);
        }
        else if(action.equals("getRequestToken")){
            String callbackURL = args.getString(0) != null ? args.getString(0) : "wmoauth://t4jsample";
            this.getRequestToken(callbackURL, callbackContext);
            return true;
        }
        else if(action.equals("getAccessToken")){
            String verifier = args.getString(0);
            this.getAccessToken(verifier, callbackContext);
        }
        else if(action.equals("setAccessToken")){
            String aToken = args.getString(0);
            String aSecret = args.getString(1);
            this.setAccessToken(aToken, aSecret, callbackContext);
            return true;
        }
        else if(action.equals("getTwitterUsername")){
            this.getUsername(callbackContext);
            return true;
        }
        else if(action.equals("sendTweet")){
            String message = args.getString(0);
            this.tweet(message, callbackContext);
            return true;
        }
        return false;
    }
    
    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
        // your init code here
    }
    
    // Set consumer token and secret from twitter
    private void init(String consumerKey, String consumerSecret ,CallbackContext callbackContext){
        twitter = new TwitterFactory().getInstance();
        twitter.setOAuthConsumer(consumerKey, consumerSecret);
        callbackContext.success();
    }
    
    // Get request URL to ask for permission
    private void getRequestToken(final String callbackURL, final CallbackContext callbackContext){
        cordova.getThreadPool().execute(new Runnable() {
            public void run() {
                try {
                    JSONObject json = new JSONObject();
                    requestToken = twitter.getOAuthRequestToken(callbackURL);
                    
                    String requestUrl = requestToken.getAuthorizationURL();
                    String token = requestToken.getToken();
                    
                    json.put("token", token);
                    json.put("url", requestUrl);
                    callbackContext.success(json);
                } catch (TwitterException e) {
                    e.printStackTrace();
                    callbackContext.error(e.getErrorMessage());
                } catch (Exception e) {
                    e.printStackTrace();
                    callbackContext.error(e.getMessage());
                }
            }
        });
    }
    
    // Get the accessToken given the verifier returned from the web login
    private void getAccessToken(String verifier, CallbackContext callbackContext){
        try{
            accessToken = twitter.getOAuthAccessToken(requestToken, verifier);
            JSONObject json = new JSONObject();
            json.put("aToken", accessToken.getToken());
            json.put("aSecret", accessToken.getTokenSecret());
            json.put("screenname", accessToken.getScreenName());
            callbackContext.success(json);
        }catch (TwitterException e) {
            e.printStackTrace();
            callbackContext.error(e.getErrorMessage());
        } catch (JSONException e) {
            e.printStackTrace();
            callbackContext.error(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            callbackContext.error(e.getMessage());
        }
        
    }
    
    // Set the access token so we can do stuff
    private void setAccessToken(String aToken, String aSecret ,CallbackContext callbackContext){
        try{
            accessToken = new AccessToken(aToken, aSecret);
            twitter.setOAuthAccessToken(accessToken);
            twitter.verifyCredentials();
            JSONObject json = new JSONObject();
            json.put("aToken", accessToken.getToken());
            json.put("aSecret", accessToken.getTokenSecret());
            json.put("screenname", accessToken.getScreenName());
            callbackContext.success(json);
        }catch (TwitterException e) {
            e.printStackTrace();
            accessToken = null;
            callbackContext.error(e.getErrorMessage());
        }catch (JSONException e) {
            e.printStackTrace();
            callbackContext.error(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            callbackContext.error(e.getMessage());
        }
        
    }
    
    // Get user screen name
    private void getUsername(CallbackContext callbackContext){
        String screenname = accessToken.getScreenName();
        callbackContext.success(screenname);
    }
    
    // Send Tweet
    private void tweet(String message, CallbackContext callbackContext){
        try{
            Status newTweet = twitter.updateStatus(message);
            callbackContext.success(Long.toString(newTweet.getId()));
        }catch (TwitterException e){
            e.printStackTrace();
            callbackContext.error(e.getErrorMessage());
        }catch (Exception e) {
            e.printStackTrace();
            callbackContext.error(e.getMessage());
        }
        
    }
    
}
