function WMTwitter(){
}

WMTwitter.prototype.getUsername = function(successCallback, errorCallback){
	cordova.exec(successCallback, this._getErrorCallback(errorCallback, "getTwitterUsername"), "WMTwitter", "getTwitterUsername", []);
}

WMTwitter.prototype.sendTweet = function(msg,successCallback, errorCallback){
	cordova.exec(successCallback, this._getErrorCallback(errorCallback, "sendTweet"), "WMTwitter", "sendTweet", [msg]);	
}



// Android Specific methods
WMTwitter.prototype.init = function(consumerKey, consumerSecret, successCallback, errorCallback){
	cordova.exec(successCallback, this._getErrorCallback(errorCallback, "init"), "WMTwitter", "init", [consumerKey, consumerSecret]);
}

WMTwitter.prototype.getRequestToken = function(callbackURL, successCallback, errorCallback){
	cordova.exec(successCallback, this._getErrorCallback(errorCallback, "getRequestToken"), "WMTwitter", "getRequestToken", [callbackURL]);
}

WMTwitter.prototype.getAccessToken = function(verifier, successCallback, errorCallback){
	cordova.exec(successCallback, this._getErrorCallback(errorCallback, "getAccessToken"), "WMTwitter", "getAccessToken", [verifier]);
}

WMTwitter.prototype.setAccessToken = function(aToken, aSecret, successCallback, errorCallback){
	cordova.exec(successCallback, this._getErrorCallback(errorCallback, "setAccessToken"), "WMTwitter", "setAccessToken", [aToken, aSecret]);
}
// END Android

WMTwitter.prototype._getErrorCallback = function (ecb, functionName) {
  if (typeof ecb === 'function') {
    return ecb;
  } else {
    return function (result) {
      console.log("The injected error callback of '" + functionName + "' received: " + JSON.stringify(result));
    }
  }
};
WMTwitter.install = function () {
  if (!window.plugins) {
    window.plugins = {};
  }

  window.plugins.wmtwitter = new WMTwitter();
  return window.plugins.wmtwitter;
};
cordova.addConstructor(WMTwitter.install);
